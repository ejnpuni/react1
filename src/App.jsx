import logo from "./logo.svg";
import "./App.css";
import Login from "./pages/login";
import ThemeProvider from "./utils/theme.context";
import Contacto from "./pages/contacto";
import Portada from "./pages/portada";
import Info from "./pages/informacion";

function App() {
  return (
    <ThemeProvider _theme="light">
      <div className="App">

        <div className="btn btn-secondary w-100"
          >Proyecto Ever Navarro Car IV</div>

        <Portada />,
        <Login />,
        <Contacto />,
        <Info />

        <div className="btn btn-secondary w-100"
          >Proyecto Ever Navarro Car IV</div>

      </div>

    </ThemeProvider>

  );
}
export default App;




