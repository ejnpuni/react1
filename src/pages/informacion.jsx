
import React, { useContext, useRef } from "react";
import InfoForm from "../components/informacion-form";
import { ThemeContext } from "../utils/theme.context";

const Info = () => {
    const ref = useRef(null);
    const context = useContext(ThemeContext);

    const info = (a) => {
        console.log(a);
    };

    let ds = false;
    if (ref.current) {
        ds = ref.current.isSubmitting;
    }
    const text_class =
        context.theme === "dark" ? "text-white text-center" : "text-center";

    return (

        <div className="w-100 h-100 d-flex align-items-center justify-content-center">
            <div className={"container-fluid bg-" + context.theme}>
                <div className="row">
                    <div
                        className="col-12"
                        style={{
                            backgroundSize: "cover",

                        }}
                    ></div>
                    <div className="col-12 p-5">
                        <button
                            className="btn btn-primary w-100"
                            disabled={ds}
                            onClick={() => {
                                console.log("ok", ref.current);
                                if (ref.current) {
                                    ref.current.submitForm();
                                }
                                context.toggle_theme("dark");
                            }}
                        >
                            Informacion
                        </button>

                        <h1 className={text_class}>There are many variations of passages of Lorem Ipsum available,
                            but the majority have suffered alteration in some form, by injected humour, or randomised
                            words which don't look even slightly believable. If you are going to use a passage of Lorem
                            Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
                            All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,
                            making this the first true generator on the Internet. It uses a dictionary of over 200 Latin
                            words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks
                            reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour,
                            or non-characteristic words etc.</h1>
                        <InfoForm onSubmit={info} myRef={ref} />

                    </div>
                </div>
            </div>
        </div>
    );
};

export default Info;//Info