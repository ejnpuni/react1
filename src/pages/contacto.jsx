
import React, { useContext, useRef } from "react";
import ContactoForm from "../components/contacto-form";
import { ThemeContext } from "../utils/theme.context";

const Contacto = () => {
  const ref = useRef(null);
  const context = useContext(ThemeContext);

  const contacto = (a) => {
    console.log(a);
  };

  let ds = false;
  if (ref.current) {
    ds = ref.current.isSubmitting;
  }
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  return (
    
    <div className="w-100 h-100 d-flex align-items-center justify-content-center">
      <div className={"container-fluid bg-" + context.theme}>
        <div className="row">
          <div
            className="col-6"
            style={{
              backgroundSize: "cover",
              backgroundImage: "url(https://picsum.photos/id/1/800/300)",

            }}
          ></div>
          <div className="col-6 p-5">
            <h1 className={text_class}>Contacto</h1>
            <ContactoForm onSubmit={contacto} myRef={ref} />
            <button
              className="btn btn-primary w-100"
              disabled={ds}
              onClick={() => {
                console.log("ok", ref.current);
                if (ref.current) {
                  ref.current.submitForm();
                }
                context.toggle_theme("dark");
              }}
            >
              Enviar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contacto;//Contacto
