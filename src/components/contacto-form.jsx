import { Field, Form, Formik } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";

const ContactoForm = ({ onSubmit, myRef }) => {
  const context = useContext(ThemeContext);
  const inicial_data = {
    user_name: "",
    cellphone:"",
    country:"",
    texto: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  const text_class = context.theme === "dark" ? "text-white" : "";

  return (
    <Formik
      initialValues={inicial_data}
      onSubmit={submit}
      innerRef={myRef}
    >
      {({}) => {
        return (
          <Form>
            <div className="form-group mb-3">
              <label className={text_class} htmlFor="username">
                Nombre de usuario
              </label>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="user_name"
                placeholder="Ingrese nombre de usuario"
              />
            </div>
            <div className="form-group mb-3">
              <label className={text_class} htmlFor="cellphone">
                Telefono
              </label>
              <Field
                className="form-control"
                id="cellphone"
                type="text"
                name="cellphone"
                placeholder="ingrese su numero de telefono"
              />
            </div>

            <div className="form-group mb-3">
              <label className={text_class} htmlFor="country">
                Pais/Region
              </label>
              <Field
                className="form-control"
                id="country"
                type="text"
                name="country"
                placeholder="ingrese su pais de residencia"
              />
            </div>
            <div className="form-group mb-3">
              <label className={text_class} htmlFor="texto">
                Descripcion
              </label>
              <Field
                className="form-control"
                id="texto"
                type="text"
                name="texto"
                placeholder="Descripcion personal"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default ContactoForm; 

