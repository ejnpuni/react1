import { Field, Form, Formik } from "formik";
import { useContext } from "react";
import { ThemeContext } from "../utils/theme.context";

const InfoForm = ({ onSubmit, myRef }) => {
  const context = useContext(ThemeContext);
  const inicial_data = {
    user_name: "",
    password: "",
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  const text_class = context.theme === "dark" ? "text-white" : "";

  return (
    <Formik
      initialValues={inicial_data}
      onSubmit={submit}
      innerRef={myRef}
    >
      {({}) => {
        return (
          <Form>
            
            
          </Form>
        );
      }}
    </Formik>
  );
};

export default InfoForm;//InfoFormm
